/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Practicas;

import java.awt.Component;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
/**
 *
 * @author elias
 */
public class Practica11 extends javax.swing.JFrame {

    Connection conn = null;
    DefaultTableModel tm = null;
    int renSel = 0;
    int colSel = 0;

    /**
     * Creates new form Practica11
     */
    public Practica11() {
        
        initComponents();
        
        this.jTable1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        tm = (DefaultTableModel) this.jTable1.getModel();
        
        try {
            this.loadDriver();
        } catch (Exception ex) {
            // handle the error
            JOptionPane.showMessageDialog(rootPane,
                    "No se puede cargar el driver MySQL: " + ex.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE
            );
            System.exit(0);
        };

        this.connectToBD();
        this.consultar();
    }

    void loadDriver() throws Exception {
        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
        System.out.println("Driver cargado exitosamente");
    }

    void connectToBD() {
        try {
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/encuesta?zeroDateTimeBehavior=CONVERT_TO_NULL"
                    + "&user=encuesta_user&password=encuesta_pass");

            System.out.println("Conectado!");

        } catch (SQLException ex) {

            JOptionPane.showMessageDialog(rootPane,
                    "Error en la conexion de BD: " + ex.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE
            );

            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());

            System.exit(0);
        }
    }
    
    void consultar(){
        String sQuery = "SELECT id,sSisOper,cProgra,cDiseno,cAdmon,iHoras FROM respuestas";
        ResultSet rset;
        try {
            Statement stmt = this.conn.createStatement();
            
            rset = stmt.executeQuery(sQuery);
            
            tm.setRowCount(0);
            
            while(rset.next()){
                tm.addRow(new Object[]{rset.getInt("id"),
                                       rset.getString("sSisOper"),
                                       rset.getString("cProgra"),
                                       rset.getString("cDiseno"),
                                       rset.getString("cAdmon"),
                                       rset.getInt("iHoras"),
                });
                
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(Practica11.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        btnActualizar = new javax.swing.JButton();
        btnEditar = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Id", "Sis.Operativo.", "Progra.", "Diseño.", "Admon.", "Horas"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true, true, true, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTable1);

        btnActualizar.setText("Actualizar");
        btnActualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarActionPerformed(evt);
            }
        });

        btnEditar.setText("Editar");
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });

        btnEliminar.setText("Eliminar");
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });

        btnGuardar.setText("Guardar");
        btnGuardar.setEnabled(false);
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        btnCancelar.setText("Cancelar");
        btnCancelar.setEnabled(false);
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(64, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 452, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(45, 45, 45))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnActualizar)
                        .addGap(248, 248, 248))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnEditar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnGuardar)
                        .addGap(7, 7, 7)
                        .addComponent(btnCancelar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEliminar)
                        .addGap(46, 46, 46))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(30, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnEditar)
                    .addComponent(btnEliminar)
                    .addComponent(btnGuardar)
                    .addComponent(btnCancelar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnActualizar)
                .addGap(25, 25, 25))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnActualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarActionPerformed
        // TODO add your handling code here:
        this.consultar();
    }//GEN-LAST:event_btnActualizarActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        // TODO add your handling code here:
        int renglon = this.jTable1.getSelectedRow();
        String id = "";
        Statement stmt;
        
        if (renglon>=0){
            
            id = tm.getValueAt(renglon,0).toString();
            
            try {
                stmt = conn.createStatement();
                
                stmt.execute("DELETE FROM respuestas WHERE id = "+id);
                this.consultar();
                
            } catch (SQLException ex) {
                Logger.getLogger(Practica11.class.getName()).log(Level.SEVERE, null, ex);
            }                                
        } else {
            JOptionPane.showMessageDialog(rootPane,
                    "Seleccione un registro a eliminar",
                    "Error",
                    JOptionPane.ERROR_MESSAGE
            );
        }
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        // TODO add your handling code here:
        int renglon = this.jTable1.getSelectedRow();
        int columna = this.jTable1.getSelectedColumn();
       
        String id = "";        
        
        if (renglon>=0){
            
            this.renSel = renglon;
            this.colSel = columna;
            
            this.btnGuardar.setEnabled(true);
            this.btnCancelar.setEnabled(true);
            this.btnEditar.setEnabled(false);
            this.btnEliminar.setEnabled(false);
            this.btnActualizar.setEnabled(false);
            
            if (this.jTable1.editCellAt(renglon, columna)) {
                
                Component editor = this.jTable1.getEditorComponent();
                editor.requestFocusInWindow();
                
                System.out.println("Se habilitó la edición de la columna");
            }
            
            
                             
        } else {
            JOptionPane.showMessageDialog(rootPane,
                    "Seleccione un registro a editar",
                    "Error",
                    JOptionPane.ERROR_MESSAGE
            );
        }
    }//GEN-LAST:event_btnEditarActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        // TODO add your handling code here:
         this.btnGuardar.setEnabled(false);
        this.btnCancelar.setEnabled(false);
        this.btnEditar.setEnabled(true);
        this.btnEliminar.setEnabled(true);
        this.btnActualizar.setEnabled(true);
        
        String sId;
        String sSisOper;
        String sProgra;
        String sDiseno;
        String sAdmon;
        String iHoras;
        
        Statement stmt;
        
        String sSQL = "";
        
        try {
            stmt = this.conn.createStatement();
            
            sId = this.tm.getValueAt(this.renSel, 0).toString();
            sSisOper = this.tm.getValueAt(this.renSel, 1).toString();
            sProgra = this.tm.getValueAt(this.renSel, 2).toString();
            sDiseno = this.tm.getValueAt(this.renSel, 3).toString();
            sAdmon = this.tm.getValueAt(this.renSel, 4).toString();
            iHoras = this.tm.getValueAt(this.renSel, 5).toString();
            
            sSQL = String.format("UPDATE respuestas SET sSisOper = '%s',cProgra = '%s',cDiseno = '%s',cAdmon = '%s',iHoras = %s WHERE id = %s",
                                                        sSisOper,sProgra,sDiseno,sAdmon,iHoras,sId);
            
            stmt.execute(sSQL);
            
        } catch (SQLException ex) {
            Logger.getLogger(Practica11.class.getName()).log(Level.SEVERE, null, ex);
        } 
        
        if (this.jTable1.getCellEditor()!=null){
            this.jTable1.getCellEditor().cancelCellEditing();
        }
           
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        // TODO add your handling code here:
        this.btnGuardar.setEnabled(false);
        this.btnCancelar.setEnabled(false);
        this.btnEditar.setEnabled(true);
        this.btnEliminar.setEnabled(true);
        this.btnActualizar.setEnabled(true);
        
        if (this.jTable1.getCellEditor()!=null){
            this.jTable1.getCellEditor().cancelCellEditing();
        }
    }//GEN-LAST:event_btnCancelarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Practica11.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Practica11.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Practica11.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Practica11.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Practica11().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnActualizar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}
