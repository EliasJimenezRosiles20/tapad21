/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Practicas;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author elias
 */
public class Practica8 extends javax.swing.JFrame {

    /**
     * Creates new form Practica8
     */
    public Practica8() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mnbBarraMenus = new javax.swing.JMenuBar();
        mnuArchivo = new javax.swing.JMenu();
        mniAbrir = new javax.swing.JMenuItem();
        mniSalir = new javax.swing.JMenuItem();
        mnuEditar = new javax.swing.JMenu();
        mniCopiar = new javax.swing.JMenuItem();
        mniCortar = new javax.swing.JMenuItem();
        mniPegar = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        mnuArchivo.setText("Archivos");

        mniAbrir.setText("Abrir");
        mniAbrir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mniAbrirActionPerformed(evt);
            }
        });
        mnuArchivo.add(mniAbrir);

        mniSalir.setText("Salir");
        mniSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mniSalirActionPerformed(evt);
            }
        });
        mnuArchivo.add(mniSalir);

        mnbBarraMenus.add(mnuArchivo);

        mnuEditar.setText("Editar");

        mniCopiar.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        mniCopiar.setIcon(new javax.swing.ImageIcon("C:\\Users\\elias\\OneDrive\\Imágenes\\Copiar (2).PNG")); // NOI18N
        mniCopiar.setText("Copiar");
        mniCopiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mniCopiarActionPerformed(evt);
            }
        });
        mnuEditar.add(mniCopiar);

        mniCortar.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_X, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        mniCortar.setIcon(new javax.swing.ImageIcon("C:\\Users\\elias\\OneDrive\\Imágenes\\Cortar (3).PNG")); // NOI18N
        mniCortar.setText("Cortar");
        mniCortar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mniCortarActionPerformed(evt);
            }
        });
        mnuEditar.add(mniCortar);

        mniPegar.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_V, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        mniPegar.setIcon(new javax.swing.ImageIcon("C:\\Users\\elias\\OneDrive\\Imágenes\\Pegar (2).PNG")); // NOI18N
        mniPegar.setText("Pegar");
        mniPegar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mniPegarActionPerformed(evt);
            }
        });
        mnuEditar.add(mniPegar);

        mnbBarraMenus.add(mnuEditar);

        setJMenuBar(mnbBarraMenus);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 388, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 205, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void mniAbrirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mniAbrirActionPerformed
        // TODO add your handling code here:
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
             "DOC", "doc", "docx");
        chooser.setFileFilter(filter);
        int returnVal = chooser.showOpenDialog(rootPane);
        if(returnVal == JFileChooser.APPROVE_OPTION) {
            System.out.println("You chose to open this file: " +
                 chooser.getSelectedFile().getName());
        }
    }//GEN-LAST:event_mniAbrirActionPerformed

    private void mniSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mniSalirActionPerformed
        // TODO add your handling code here:
        System.exit(0);
    }//GEN-LAST:event_mniSalirActionPerformed

    private void mniCopiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mniCopiarActionPerformed
        // TODO add your handling code here:
        JOptionPane.showMessageDialog(rootPane, "Presionaste Ctrl+C", "Titulo", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_mniCopiarActionPerformed

    private void mniCortarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mniCortarActionPerformed
        // TODO add your handling code here:
        JOptionPane.showMessageDialog(rootPane, "Presionaste Ctrl+X", "Titulo", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_mniCortarActionPerformed

    private void mniPegarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mniPegarActionPerformed
        // TODO add your handling code here:
        JOptionPane.showMessageDialog(rootPane, "Presionaste Ctrl+V", "Titulo", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_mniPegarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Practica8.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Practica8.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Practica8.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Practica8.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Practica8().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuBar mnbBarraMenus;
    private javax.swing.JMenuItem mniAbrir;
    private javax.swing.JMenuItem mniCopiar;
    private javax.swing.JMenuItem mniCortar;
    private javax.swing.JMenuItem mniPegar;
    private javax.swing.JMenuItem mniSalir;
    private javax.swing.JMenu mnuArchivo;
    private javax.swing.JMenu mnuEditar;
    // End of variables declaration//GEN-END:variables
}
