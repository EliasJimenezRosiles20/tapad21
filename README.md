# Ejemplo de Readme

Este es un Ejemplo de archivo Readme

## Autor

Jimenez Rosiles Elias
No. Control: E19021634

Proyecto de prácticas de TAP

## Installation

Este es un proyecto de prácticas de la materia de Tópicos

```bash
git clone https://bitbucket.org/ruta/etc
```

## Uso

```python
import foobar

# returns 'words'
foobar.pluralize('word')

# returns 'geese'
foobar.pluralize('goose')

# returns 'phenomenon'
foobar.singularize('phenomena')
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
